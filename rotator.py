from keras.models import load_model
from keras.optimizers import SGD
from collections import Counter

from utils import get_frames_from_videos

import numpy as np

class Rotator(object):

    def __init__(self,model_location,model_error_func,num_extraction=50):

        self.model_location = model_location
        self.error = model_error_func
        self.model = None
        self.num_extraction = num_extraction
        self.build_model()

    def build_model(self):

        self.model = load_model(self.model_location, custom_objects={'angle_error': self.error})
        self.model.compile(loss='categorical_crossentropy',
                      optimizer=SGD(lr=0.01, momentum=0.9),
                      metrics=[self.error])

    def get_video_rotation_angle(self,video_url):

        input = get_frames_from_videos(video_url,self.num_extraction)
        angles_array = self.model.predict_on_batch(input)
        rotation_angle = self.cal_video_rotation_angle(angles_array)
        return rotation_angle

    def cal_video_rotation_angle(self,angles_array):
        # get most common angle
        angles_array_max = np.argmax(angles_array,axis=1)
        angle_counter = Counter(angles_array_max.tolist())
        angle_frequency_list = angle_counter.most_common()
        top_angle_list = [angle_frequency_list[0][0]]
        temp_angle_count = angle_frequency_list[0][1]
        for i in range(1,len(angle_frequency_list)):
            if temp_angle_count == angle_frequency_list[i][1]:
                top_angle_list.append(angle_frequency_list[i][0])
            else:
                break
        #to-do ideally there should be only 1 top angle and practically all top angles must lie in a small interval - if now there is a problem
        #to-do code to check the top angle list is consistent - mean should not be so simple
        return np.mean(top_angle_list)