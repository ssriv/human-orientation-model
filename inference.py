from rotator import  Rotator
from utils import angle_error


import argparse
if __name__ == '__main__':


    parser = argparse.ArgumentParser()

    parser.add_argument("-u", "--video_url",
                        help="video_url",
                        required=True)
    args = parser.parse_args()

    video_url = args.video_url

    model_location = "models/weights_generic.28-0.10.hdf5"
    model_error_func = angle_error
    num_extraction = 50
    rotator = Rotator(model_location,model_error_func,num_extraction)

    angle = rotator.get_video_rotation_angle(video_url)
    print("FINAL ANGLE -",angle)
