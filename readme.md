# Video Orientation Model - Human 
#### Description
Deep Learning model to correctly rotate video clips with human presence at the best suitable angle. The main target for current iteration is to target review videos.
#### Use case
Video clips captured at abnormal angles are not amenable with most of the deep learning models that are trained on rightly oriented videos and images. Rotating video at a correct angle can improve the performance of vision-based models. 
#### Model Architecture
Multi-class classification model where classes are any integer value between 0 and 360. ResNet50 model pre-trained on ImageNet is used along with a dense output layer with 360 units. 
#### Datasets
##### Used 
1. HMDB51 - Human Motion Database (51 classes)
2. UCF101 - University of Central Florida (101 classes)
3. Charades
##### To be used in future iterations
1. Youplus - Review Dataset
2. Youplus - Mike App Dataset
3. ActivityNet
4. Kinetics
5. AVA Actions
6. Hollywood2
7. VGG Human Pose Estimation
3. Charades

#### Model
Download the model from this link - https://drive.google.com/open?id=144gOXjLIf5D21gTQ3YYO1-3WekgVMBs9

#### Run
###### 1. Training
1. Change the below at line number 18 in train/train_men_standing.py to the location of all training images.

     `data_path = "<location_to_all_images>"`
 
2. To start the training, run 

    `python train/train_men_standing.py`

>Images used in training are extracted from aforementioned video dataset. Dataset can be prepared by using any key frame extractor on video dataset. 
###### 2. Inference 
1. Change the below at line number 16 in inference.py to the location of model weights.'

    `model_location = "<model_hdf5_file_location>"`
2. To get rotation angle on a video - run

    `python inference.py -u <video_url>`
#### Results
###### Training 
1. Error - 0.10
2. Angle loss - 2 degree
###### Validation
1. Error - 0.22
2. Angle loss - 3 degree
###### Test
1. Error ~ 0.5 to 4 depending on video set
2. Angle loss ~ 4 to 10 degree
#### Inference
Results on few human containing images
![](data/readme_images/result.png)
